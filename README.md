# Calendar Generator for Alumnia Events
This project can be used to generate an `.ics` file with all tasks that need to be performed for an Alumnia event.


## How to install
1. Install Python 3 on your computer
3. Make sure poetry is installed: `pip install poetry`
2. Navigate to `alumnia-calendar-generator` project path
4. Install dependencies: `poetry install`

## How to change tasks
1. Edit the `tasks.csv` file in the `alumnia-calendar-generator` project path

## How to run
1. Access the virtual environment: `poetry shell`
2. Run script: `python main.py`
3. Give the requested inputs in the command line
4. An `.ics` file is generated in the format `calendar_{event_name}.ics` in the `events` folder
