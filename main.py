import pandas as pd
from pytz import timezone
from ics import Calendar, Event


tasks_df = pd.read_csv("tasks.csv")

event_name = input("What is the name of the event? ")
start_datetime_str = input("When does the event start (dd-mm-yyyy hh:mm:ss)? ")
end_datetime_str = input("When does the event end (dd-mm-yyyy hh:mm:ss)? ")


tz = timezone("Europe/Amsterdam")
start_datetime = pd.to_datetime(start_datetime_str, format="%d-%m-%Y %H:%M:%S").replace(tzinfo=tz)
end_datetime = pd.to_datetime(end_datetime_str, format="%d-%m-%Y %H:%M:%S").replace(tzinfo=tz)
start_datetime = start_datetime
end_datetime = end_datetime

c = Calendar()

event = Event()
event.name = event_name
event.begin = start_datetime
event.end = end_datetime

c.events.add(event)

for _, task in tasks_df.iterrows():
    task_date = (start_datetime - pd.Timedelta(f"{task.days_before_start_event} days")).strftime("%Y-%m-%d")
    task_begin = pd.to_datetime(f"{task_date} {task.start_time}").replace(tzinfo=tz)
    task_end = task_begin + pd.Timedelta(f"{task.hours_duration} hour")
    e = Event()
    e.name = f"{task.task} for {event_name}"
    e.begin = task_begin
    e.end = task_end
    e.description = task.description
    c.events.add(e)


event_slug = event_name.lower().replace(" ", "_")

with open(f"events/calendar_{event_slug}.ics", "w") as ics_file:
    ics_file.writelines(c)
